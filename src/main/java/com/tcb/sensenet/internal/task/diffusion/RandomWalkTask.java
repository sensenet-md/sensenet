package com.tcb.sensenet.internal.task.diffusion;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.cytoscape.model.CyNode;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import com.google.auto.value.AutoValue;
import com.tcb.sensenet.internal.aggregation.aggregators.table.RowWriter;
import com.tcb.sensenet.internal.analysis.diffusion.NetworkWalk;
import com.tcb.sensenet.internal.analysis.diffusion.NetworkWalkStrategy;
import com.tcb.sensenet.internal.analysis.diffusion.NetworkWalks;
import com.tcb.sensenet.internal.analysis.diffusion.RandomWalkMode;
import com.tcb.sensenet.internal.analysis.diffusion.RandomWalkWeightMode;
import com.tcb.sensenet.internal.analysis.diffusion.StepStrategy;
import com.tcb.sensenet.internal.analysis.diffusion.SymmetricTargetedNetworkWalk;
import com.tcb.sensenet.internal.analysis.diffusion.TargetedNetworkWalk;
import com.tcb.sensenet.internal.analysis.diffusion.WeightedRandomStep;
import com.tcb.sensenet.internal.app.AppColumns;
import com.tcb.sensenet.internal.app.AppGlobals;
import com.tcb.sensenet.internal.log.TaskLogType;
import com.tcb.sensenet.internal.log.TaskLogUtil;
import com.tcb.sensenet.internal.meta.network.MetaNetwork;
import com.tcb.sensenet.internal.task.degree.WeightedDegreeTaskConfig;
import com.tcb.sensenet.internal.util.CancellableRunner;
import com.tcb.sensenet.internal.util.Nullable;
import com.tcb.sensenet.internal.util.ObjMap;
import com.tcb.sensenet.internal.util.RandomUtil;
import com.tcb.aifgen.util.MapUtil;
import com.tcb.common.util.SafeMap;
import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyNetworkAdapter;
import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyRowAdapter;
import com.tcb.cytoscape.cyLib.log.LogBuilder;
import com.tcb.cytoscape.cyLib.log.ParameterReporter;

public class RandomWalkTask extends AbstractTask {

	private AppGlobals appGlobals;
	private Config config;
	
	@AutoValue
	public static abstract class Config implements ParameterReporter {
		
		public static Config create(Long seed, RandomWalkMode randomWalkMode,
				RandomWalkWeightMode randomWalkWeightMode,
				Long sourceSUID, Long targetSUID, Integer maxSteps,
				String weightColumn, Double restartProb, Integer numRuns,
				RowWriter rowWriter){
			return new AutoValue_RandomWalkTask_Config(seed,
					randomWalkMode,randomWalkWeightMode,sourceSUID,targetSUID,
					maxSteps,weightColumn,restartProb,numRuns,
					rowWriter);
		}
		
		public abstract Long getSeed();
		public abstract RandomWalkMode getRandomWalkMode();
		public abstract RandomWalkWeightMode getRandomWalkWeightMode();
		@Nullable
		public abstract Long getSourceSUID();
		@Nullable
		public abstract Long getTargetSUID();
		public abstract Integer getMaxSteps();
		@Nullable
		public abstract String getWeightColumn();
		public abstract Double getRestartProbability();
		public abstract Integer getNumRuns();
		public abstract RowWriter getRowWriter();
		
		public TaskLogType getTaskLogType(){
			return TaskLogType.DIFFUSION;
		}
	}
	
	
	public RandomWalkTask(
			Config config,
			AppGlobals appGlobals){
		this.config = config;
		this.appGlobals = appGlobals;
		this.cancelled = false;
	}


	@Override
	public void run(TaskMonitor arg0) throws Exception {
		CyNetworkAdapter network = appGlobals.applicationManager.getCurrentNetwork();
		MetaNetwork metaNetwork = appGlobals.state.metaNetworkManager.getCurrentMetaNetwork();
		
		if(config.getSourceSUID() == null) throw new IllegalArgumentException("No source node selected");
		if(config.getMaxSteps() < 0) throw new IllegalArgumentException("Max steps must not be negative");
		if(config.getNumRuns() < 1) throw new IllegalArgumentException("Number of runs must be at least 1");
		if(config.getRestartProbability() < 0.) throw new IllegalArgumentException("Restart probability must not be negative");
		
		LogBuilder log = TaskLogUtil.createTaskLog(metaNetwork, config.getTaskLogType(),
				appGlobals.state.logManager);
		TaskLogUtil.startTaskLog(log, config.getTaskLogType(), metaNetwork, network, config);
				
		CyNode source = network.getNode(config.getSourceSUID());
		CyNode target = config.getTargetSUID() == null ? null : network.getNode(config.getTargetSUID());
		boolean countOnlyOnce = true;
		
		RandomUtil rnd = new RandomUtil(config.getSeed());
		NetworkWalkStrategy walkStrategy = getWalkStrategy(rnd,network,target);
		NetworkWalks networkWalks = new NetworkWalks();
		SafeMap<CyNode,Double> weights = getWeights(
				config.getRandomWalkWeightMode(),
				network, config.getWeightColumn());
		StepStrategy stepStrategy = new WeightedRandomStep(rnd, weights);
		
		Integer stepsPerRun = config.getMaxSteps();
		Integer nRuns = config.getNumRuns();
		
		SafeMap<CyNode,Integer> visited = CancellableRunner.run(() ->
			networkWalks.runNetworkWalks(walkStrategy, stepStrategy, nRuns,
										network, source, stepsPerRun, countOnlyOnce),
			() -> cancelled == true,
			() -> walkStrategy.cancel());
		
		if(config.getRandomWalkMode().equals(RandomWalkMode.TARGETED_SYMMETRIC)) {
			NetworkWalkStrategy reverseWalkStrategy = getWalkStrategy(rnd,network,source);
			SafeMap<CyNode,Integer> visitedReverse = CancellableRunner.run(() ->
				networkWalks.runNetworkWalks(reverseWalkStrategy, stepStrategy, nRuns,
											network, target, stepsPerRun, countOnlyOnce),
				() -> cancelled == true,
				() -> reverseWalkStrategy.cancel());
			
			for(Entry<CyNode,Integer> kv:visitedReverse.entrySet()) {
				visited.compute(kv.getKey(), (k,v) -> (v == null) ? kv.getValue() : v + kv.getValue());
			}
		}
				
		RowWriter rowWriter = config.getRowWriter();
		for(CyNode n:network.getNodeList()){
			CyRowAdapter row = network.getRow(n);
			Integer count = visited.getOrDefault(n, 0);
			ObjMap m = new ObjMap();
			m.put("visited", (double)count);
			rowWriter.write(row, m);
		}
		
		TaskLogUtil.finishTaskLog(log);
	}
	
	private NetworkWalkStrategy getWalkStrategy(RandomUtil rnd, CyNetworkAdapter network,
			CyNode target){
		switch(config.getRandomWalkMode()){
		case DEFAULT: 
			return new NetworkWalk(rnd, config.getRestartProbability());
		case TARGETED:
			if(target == null) throw new IllegalArgumentException("No target node selected");
			return new TargetedNetworkWalk(rnd, config.getRestartProbability(), target);
		case TARGETED_SYMMETRIC:
			if(target == null) throw new IllegalArgumentException("No target node selected");
			return new TargetedNetworkWalk(rnd, config.getRestartProbability(), target);
		default: throw new IllegalArgumentException();
		}
	}
				
	private SafeMap<CyNode,Double> getWeights(
			RandomWalkWeightMode randomWalkWeightMode,
			CyNetworkAdapter network, 
			String weightColumn){
		SafeMap<CyNode,Double> weights = new SafeMap<>();
		switch(randomWalkWeightMode) {
		case WEIGHTED:
			if(weightColumn == null) throw new IllegalArgumentException("Must select weight column");
			for(CyNode n:network.getNodeList()) {
				Double weight = network.getRow(n).getRaw(weightColumn, Double.class);
				weights.put(n, weight);
			}
			break;
		case UNWEIGHTED:
			for(CyNode n:network.getNodeList()) {
				weights.put(n, 1.);
			}
			break;
		default: throw new IllegalArgumentException();
		}
		return weights;
	}

}
