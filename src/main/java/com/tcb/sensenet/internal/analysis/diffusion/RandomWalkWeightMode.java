package com.tcb.sensenet.internal.analysis.diffusion;

public enum RandomWalkWeightMode {
	UNWEIGHTED, WEIGHTED;
	
	public String toString(){
		switch(this){
		case UNWEIGHTED: return "Unweighted";
		case WEIGHTED: return "Weighted";
		default: throw new IllegalArgumentException();
		}
	}
}
