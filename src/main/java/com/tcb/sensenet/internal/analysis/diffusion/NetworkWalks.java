package com.tcb.sensenet.internal.analysis.diffusion;

import java.util.List;
import java.util.stream.Collectors;

import org.cytoscape.model.CyNode;

import com.tcb.common.util.SafeMap;
import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyNetworkAdapter;

public class NetworkWalks {
	
	public volatile boolean cancelled = false;
	
	public void cancel() {
		cancelled = true;
	}
	
	public SafeMap<CyNode,Integer> runNetworkWalks(
			NetworkWalkStrategy walkStrategy,
			StepStrategy stepStrategy,
			int nRuns, CyNetworkAdapter network,
			CyNode source, int stepsPerRun,
			boolean countOncePerRun){
		SafeMap<CyNode,Integer> r = new SafeMap<>();
		for(int run=0;run<nRuns;run++) {
			List<CyNode> path = walkStrategy.run(stepStrategy, network, source, stepsPerRun);
			if(countOncePerRun) {
				path = path.stream().distinct().collect(Collectors.toList());
			}
			for(CyNode n:path) 
				r.compute(n, (k,v) -> (v == null) ? 1 : v + 1);
			if(cancelled) return r;
		}
		return r;
	}
			

}
