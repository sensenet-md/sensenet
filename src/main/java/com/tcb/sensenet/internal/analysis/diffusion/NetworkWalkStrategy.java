package com.tcb.sensenet.internal.analysis.diffusion;

import java.util.List;

import org.cytoscape.model.CyNode;

import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyNetworkAdapter;

public interface NetworkWalkStrategy {
	public List<CyNode> run(StepStrategy stepStrategy, CyNetworkAdapter network, CyNode current, int steps);
	public void cancel();
}
