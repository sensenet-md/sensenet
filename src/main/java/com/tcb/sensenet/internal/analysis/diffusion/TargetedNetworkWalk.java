package com.tcb.sensenet.internal.analysis.diffusion;

import java.util.List;

import org.cytoscape.model.CyNode;

import com.tcb.sensenet.internal.util.RandomUtil;

public class TargetedNetworkWalk extends NetworkWalk {
	
	protected CyNode target;
	
	public TargetedNetworkWalk(RandomUtil rnd, double restartProb, CyNode target) {
		super(rnd,restartProb);
		this.target = target;
	}
	
	@Override
	protected Boolean isFinished(List<CyNode> path, CyNode current) {
		return target.equals(current);
	}

	@Override
	protected Boolean shouldRetry(List<CyNode> path, CyNode current) {
		return true;
	}

	@Override
	protected Boolean shouldRestartPath(List<CyNode> path, CyNode current) {
		return true;
	}
	
	public void setTarget(CyNode newTarget) {
		this.target = newTarget;
	}

}
