package com.tcb.sensenet.internal.analysis.diffusion;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNode;

import com.tcb.common.util.SafeMap;
import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyNetworkAdapter;
import com.tcb.sensenet.internal.util.RandomUtil;

public class WeightedRandomStep implements StepStrategy {
	
	private RandomUtil rnd;
	private SafeMap<CyNode,Double> weights;

	public WeightedRandomStep(RandomUtil rnd, SafeMap<CyNode,Double> weights) {
		this.rnd = rnd;
		this.weights = weights;
	}
	
	@Override
	public CyNode next(CyNetworkAdapter network, CyNode current) {
		// Note: Neighbors can appear multiple times if connected by more than one edge.
		List<CyNode> neighbors = network.getNeighborList(current, CyEdge.Type.ANY).stream()
				.distinct()
				.collect(Collectors.toList());
		Collections.sort(neighbors, Comparator.comparing(CyNode::getSUID));
		if(neighbors.isEmpty()) throw new IllegalArgumentException("Neighbor list empty");
		List<Double> weightsList = neighbors.stream()
				.map(n -> weights.get(n))
				.collect(Collectors.toList());
		CyNode result = rnd.pickRandom(neighbors, weightsList);
		return result;
	}	
}
