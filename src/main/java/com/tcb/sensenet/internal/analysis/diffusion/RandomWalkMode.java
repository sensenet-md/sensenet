package com.tcb.sensenet.internal.analysis.diffusion;

public enum RandomWalkMode {
	DEFAULT, TARGETED, TARGETED_SYMMETRIC;
	
	public String toString(){
		switch(this){
		case DEFAULT: return "Default";
		case TARGETED: return "Targeted";
		case TARGETED_SYMMETRIC: return "Targeted-Symmetric";
		default: throw new IllegalArgumentException();
		}
		
	}
}
