package com.tcb.sensenet.internal.analysis.diffusion;

import java.util.ArrayList;
import java.util.List;

import org.cytoscape.model.CyNode;

import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyNetworkAdapter;
import com.tcb.sensenet.internal.util.RandomUtil;

public class NetworkWalk implements NetworkWalkStrategy {
	
	protected RandomUtil rnd;
	protected double restartProb;
	protected StepStrategy stepStrategy;
	
	public volatile boolean cancelled = false;
	
	public NetworkWalk(RandomUtil rnd, double restartProb) {
		this.rnd = rnd;
		this.restartProb = restartProb;
	}

	protected Boolean isFinished(List<CyNode> path, CyNode current) {
		return false;
	}

	protected Boolean shouldRetry(List<CyNode> path, CyNode current) {
		return false;
	}

	protected Boolean shouldRestartPath(List<CyNode> path, CyNode current) {
		return false;
	}
	
	@Override
	public void cancel() {
		cancelled = true;
	}

	@Override
	public List<CyNode> run(StepStrategy stepStrategy, CyNetworkAdapter network, CyNode start, int steps) {
		List<CyNode> result = new ArrayList<>();
		CyNode n;
		do {
			n = start;
			result.clear();
			result.add(n);
			for(int i=0;i<steps;i++) {
				if(cancelled) return result;
				if(isFinished(result,n)) return result;
				Double r = rnd.nextDouble();
				if(restartProb > r) {
					if(shouldRestartPath(result, n)) result.clear();
					n = start;
					result.add(n);
					continue;
				}
				n = stepStrategy.next(network, n);
				result.add(n);
			}
		} while(shouldRetry(result,n));
		return result;
	}

}
