package com.tcb.sensenet.internal.analysis.diffusion;

import java.util.List;

import org.cytoscape.model.CyNode;

import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyNetworkAdapter;
import com.tcb.sensenet.internal.util.RandomUtil;

public class SymmetricTargetedNetworkWalk extends TargetedNetworkWalk {
	
	public SymmetricTargetedNetworkWalk(RandomUtil rnd,
			double restartProb, CyNode target) {
		super(rnd,restartProb,target);
	}
	
	@Override
	public List<CyNode> run(StepStrategy stepStrategy, CyNetworkAdapter network, CyNode start, int steps) {
		List<CyNode> path = super.run(stepStrategy, network, start, steps);
		
		TargetedNetworkWalk reverseWalk = 
				new TargetedNetworkWalk(this.rnd, this.restartProb, start);
		reverseWalk.setTarget(start);
		
		path.addAll(reverseWalk.run(stepStrategy, network, target, steps));
		
		return path;
	}

}
