package com.tcb.sensenet.internal.analysis.diffusion;

import org.cytoscape.model.CyNode;

import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyNetworkAdapter;

public interface StepStrategy {
	public CyNode next(CyNetworkAdapter network, CyNode current);
}
