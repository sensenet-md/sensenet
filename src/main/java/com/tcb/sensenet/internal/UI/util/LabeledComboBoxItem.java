package com.tcb.sensenet.internal.UI.util;

import java.util.function.Function;

public class LabeledComboBoxItem<T> {
	private T value;
	private Function<T,String> string_f;
	
	public LabeledComboBoxItem(T value, Function<T,String> string_f) {
		this.value = value;
		this.string_f = string_f;
	}
	
	@Override
	public String toString() {
		return string_f.apply(value);
	}
	
	public T getValue() {
		return value;
	}

}
