package com.tcb.sensenet.internal.UI.panels.analysisPanel.diffusion;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ItemEvent;
import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import org.apache.commons.lang3.StringUtils;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.util.swing.FileChooserFilter;
import org.cytoscape.util.swing.FileUtil;
import org.cytoscape.work.TaskIterator;

import com.google.common.collect.ImmutableList;
import com.tcb.sensenet.internal.UI.util.DefaultDialog;
import com.tcb.sensenet.internal.UI.util.FileButton;
import com.tcb.sensenet.internal.UI.util.LabeledComboBoxItem;
import com.tcb.sensenet.internal.UI.util.LabeledParametersPanel;
import com.tcb.sensenet.internal.UI.util.SingletonErrorDialog;
import com.tcb.sensenet.internal.aggregation.aggregators.table.RandomWalkWriter;
import com.tcb.sensenet.internal.aggregation.aggregators.table.RowWriter;
import com.tcb.sensenet.internal.analysis.diffusion.RandomWalkMode;
import com.tcb.sensenet.internal.analysis.diffusion.RandomWalkWeightMode;
import com.tcb.sensenet.internal.app.AppColumns;
import com.tcb.sensenet.internal.app.AppGlobals;
import com.tcb.sensenet.internal.data.NetworkColumnStatistics;
import com.tcb.sensenet.internal.data.rows.RowStatistics;
import com.tcb.sensenet.internal.meta.network.MetaNetwork;
import com.tcb.sensenet.internal.plot.color.BiColorScale;
import com.tcb.sensenet.internal.plot.color.ColorScale;
import com.tcb.sensenet.internal.plot.color.TriColorScale;
import com.tcb.sensenet.internal.properties.AppProperties;
import com.tcb.sensenet.internal.properties.AppProperty;
import com.tcb.sensenet.internal.selection.InvalidSelectionException;
import com.tcb.sensenet.internal.selection.TwoNodeSelection;
import com.tcb.sensenet.internal.task.diffusion.RandomWalkTask;
import com.tcb.sensenet.internal.task.diffusion.factories.ActionRandomWalkTaskFactory;
import com.tcb.sensenet.internal.task.export.matrix.ExportNetworkMatrixTaskConfig;
import com.tcb.sensenet.internal.task.export.matrix.factories.ExportNetworkMatrixTaskFactory;
import com.tcb.sensenet.internal.task.plot.factories.ShowNetworkMatrixPlotFrameTaskFactory;
import com.tcb.sensenet.internal.task.plot.matrix.ShowNetworkMatrixPlotFrameTaskConfig;
import com.tcb.sensenet.internal.util.DialogUtil;
import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyNetworkAdapter;
import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyRootNetworkAdapter;
import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyRowAdapter;
import com.tcb.cytoscape.cyLib.data.Columns;
import com.tcb.cytoscape.cyLib.data.DefaultColumns;
import com.tcb.aifgen.importer.TimelineType;

public class RandomWalkDialog extends DefaultDialog {
	
	private JComboBox<RandomWalkMode> walkModeBox;
	private JComboBox<RandomWalkWeightMode> walkWeightModeBox;
	private JComboBox<String> sourceBox;
	private JComboBox<String> targetBox;
	private JTextField restartProbBox;
	private JTextField maxStepsBox;
	private JTextField numRunsBox;
	private JComboBox<String> weightColumnBox;
	private JTextField randomSeedBox;

	private AppGlobals appGlobals;
	
	private static final AppProperty defaultWeightColumnProperty = 
			AppProperty.RANDOM_WALK_DEFAULT_WEIGHT_COLUMN;
	private static final AppProperty defaultRestartProbProperty =
			AppProperty.RANDOM_WALK_DEFAULT_RESTART_PROBABILITY;
	private static final AppProperty defaultMaxStepsProperty =
			AppProperty.RANDOM_WALK_DEFAULT_MAX_STEPS;
	private static final AppProperty defaultNumRunsProperty =
			AppProperty.RANDOM_WALK_DEFAULT_NUM_RUNS;
	private static final AppProperty defaultWalkModeProperty =
			AppProperty.RANDOM_WALK_DEFAULT_WALK_MODE;
	private static final AppProperty defaultSourceProperty =
			AppProperty.RANDOM_WALK_DEFAULT_SOURCE;
	private static final AppProperty defaultTargetProperty =
			AppProperty.RANDOM_WALK_DEFAULT_TARGET;
	private static final AppProperty defaultWeightModeProperty =
			AppProperty.RANDOM_WALK_DEFAULT_WEIGHT_MODE;

	public RandomWalkDialog(AppGlobals appGlobals){
		this.appGlobals = appGlobals;
		
		addGeneralOptionsPanel(this);
		
		this.add(
				DialogUtil.createActionPanel(this::confirm, this::dispose),
				getDefaultDialogConstraints());
		
		this.pack();
	}

	private void confirm(){
		String weightColumn = (String) weightColumnBox.getSelectedItem();
		CyNetworkAdapter network = appGlobals.state.metaNetworkManager.getCurrentNetwork();
		@SuppressWarnings("unchecked")
		CyNode sourceNode = null;
		String sourceLabel = "";
		if(sourceBox.getSelectedItem()!=null) {
			sourceLabel = (String)sourceBox.getSelectedItem();
			sourceNode = getNodeByLabel(network, sourceLabel);
		}
		CyNode targetNode = null;
		String targetLabel = "";
		if(targetBox.isEnabled() && targetBox.getSelectedItem()!=null) {
			targetLabel = (String)targetBox.getSelectedItem();
			targetNode = getNodeByLabel(network, targetLabel);
		}
		Long sourceSUID = null;
		if(sourceNode!=null) sourceSUID = sourceNode.getSUID();
		Long targetSUID = null;
		if(targetNode!=null) targetSUID = targetNode.getSUID();
		
		Long randomSeed = Long.parseLong(randomSeedBox.getText());
									
		Integer numRuns = Integer.parseInt(numRunsBox.getText());
		Integer maxSteps = Integer.parseInt(maxStepsBox.getText());
		
		Double restartProb = Double.parseDouble(restartProbBox.getText());
		RandomWalkMode walkMode = (RandomWalkMode) walkModeBox.getSelectedItem();
		RandomWalkWeightMode weightMode = (RandomWalkWeightMode) walkWeightModeBox.getSelectedItem();
				
		appGlobals.appProperties.set(defaultWeightColumnProperty, weightColumn);
		appGlobals.appProperties.set(defaultNumRunsProperty, numRuns.toString());
		appGlobals.appProperties.set(defaultWalkModeProperty, walkMode.name());
		appGlobals.appProperties.set(defaultMaxStepsProperty, maxSteps.toString());
		appGlobals.appProperties.set(defaultRestartProbProperty, restartProb.toString());
		appGlobals.appProperties.set(defaultSourceProperty, sourceLabel);
		appGlobals.appProperties.set(defaultTargetProperty, targetLabel);
		appGlobals.appProperties.set(defaultWeightModeProperty, weightMode.name());
		
		RowWriter rowWriter = new RandomWalkWriter();
		
		RandomWalkTask.Config config = RandomWalkTask.Config.create(
				randomSeed, walkMode,weightMode, sourceSUID,targetSUID,
				maxSteps,weightColumn,restartProb,numRuns,rowWriter);
		TaskIterator it = 
				new ActionRandomWalkTaskFactory(appGlobals)
				.createTaskIterator(config);
		appGlobals.taskManager.execute(it);
		this.dispose();
	}
			
	private void addGeneralOptionsPanel(Container target){
		LabeledParametersPanel p = new LabeledParametersPanel();
				
		AppProperties appProperties = appGlobals.appProperties;
		MetaNetwork metaNetwork = appGlobals.state.metaNetworkManager.getCurrentMetaNetwork();
		CyNetworkAdapter network = appGlobals.state.metaNetworkManager.getCurrentNetwork();
		
		String[] nodeNumericColumns = getNodeNumericColumns(metaNetwork);
			
		walkModeBox = p.addChoosableParameter("Walk mode", RandomWalkMode.values(),
				appProperties.getEnumOrDefault(RandomWalkMode.class, defaultWalkModeProperty));
		walkWeightModeBox = p.addChoosableParameter("Weighting mode", RandomWalkWeightMode.values(),
				appProperties.getEnumOrDefault(RandomWalkWeightMode.class, defaultWeightModeProperty));
		weightColumnBox = p.addChoosableParameter("Weight column",
				nodeNumericColumns, appProperties.getOrDefault(defaultWeightColumnProperty));
		
		String[] labels = getSortedNodeLabels(network);
		
		sourceBox = p.addChoosableParameter("Source node", labels, appProperties.getOrDefault(defaultSourceProperty));
		targetBox = p.addChoosableParameter("Target node", labels, appProperties.getOrDefault(defaultTargetProperty));
		
		maxStepsBox = p.addTextParameter("Max steps", appProperties.getOrDefault(defaultMaxStepsProperty));
		restartProbBox = p.addTextParameter("Restart probability", appProperties.getOrDefault(defaultRestartProbProperty));
		numRunsBox = p.addTextParameter("Num runs", appProperties.getOrDefault(defaultNumRunsProperty));
		randomSeedBox = p.addTextParameter("Random seed", Integer.toString(new Random().nextInt()));
		
		walkModeBox.addItemListener((ItemEvent e) -> {
			if(e.getStateChange() != ItemEvent.SELECTED) return;
			Object o = e.getItem();
			if(o == null) return;
			RandomWalkMode walkMode = (RandomWalkMode)o;
			update(walkMode);
		});
		
		walkWeightModeBox.addItemListener((ItemEvent e) -> {
			if(e.getStateChange() != ItemEvent.SELECTED) return;
			Object o = e.getItem();
			if(o == null) return;
			RandomWalkWeightMode weightMode = (RandomWalkWeightMode)o;
			update(weightMode);
		});
				
		update((RandomWalkMode) walkModeBox.getSelectedItem());
		update((RandomWalkWeightMode) walkWeightModeBox.getSelectedItem());
		target.add(p);
	}
	
	private void update(RandomWalkMode walkMode) {
		if(walkMode.equals(RandomWalkMode.DEFAULT)) {
			targetBox.setEnabled(false);
			targetBox.setSelectedItem(null);
		} else {
			targetBox.setEnabled(true);
		}
	}
	
	private void update(RandomWalkWeightMode weightMode) {
		if(weightMode.equals(RandomWalkWeightMode.UNWEIGHTED)) {
			weightColumnBox.setEnabled(false);
			weightColumnBox.setSelectedItem(null);
		} else {
			weightColumnBox.setEnabled(true);
		}
	}
			
	private String[] getNodeNumericColumns(MetaNetwork metaNetwork){
		NetworkColumnStatistics nodeStat = new NetworkColumnStatistics(
				metaNetwork.getRootNetwork().getSharedNodeTable()
				);
		String[] nodeNumericColumns = nodeStat.getColumns(Double.class).stream()
				.toArray(String[]::new);
		return nodeNumericColumns;
	}
	
	private String[] getSortedNodeLabels(CyNetworkAdapter network) {
		String[] nodeLabels = network.getNodeList().stream()
				.sorted((a,b) -> network.getRow(a).get(AppColumns.LABEL, String.class)
						.compareTo(network.getRow(b).get(AppColumns.LABEL, String.class)))
				.sorted((a,b) -> network.getRow(a).get(AppColumns.RESINDEX_LABEL, Integer.class)
						.compareTo(network.getRow(b).get(AppColumns.RESINDEX_LABEL, Integer.class)))
				.map(n -> network.getRow(n).get(AppColumns.LABEL, String.class))
				.toArray(String[]::new);
		return nodeLabels;
	}
	
	private CyNode getNodeByLabel(CyNetworkAdapter network, String label) {
		CyNode result = null;
		for(CyNode n:network.getNodeList()) {
			String nodeLabel = network.getRow(n).get(AppColumns.LABEL, String.class);
			if(label.equals(nodeLabel)) {
				if(result==null) result = n;
				else throw new IllegalArgumentException("Duplicate labels found: " + nodeLabel);
			}
		}
		if(result==null) throw new IllegalArgumentException("Could not find node with specified label");
		return result;
	}
}
