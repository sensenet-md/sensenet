package analysis.diffusion;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.cytoscape.model.CyNode;
import org.junit.Before;
import org.junit.Test;

import com.tcb.sensenet.internal.analysis.diffusion.NetworkWalk;
import com.tcb.sensenet.internal.analysis.diffusion.NetworkWalkStrategy;
import com.tcb.sensenet.internal.analysis.diffusion.NetworkWalks;
import com.tcb.sensenet.internal.analysis.diffusion.StepStrategy;
import com.tcb.sensenet.internal.analysis.diffusion.WeightedRandomStep;
import com.tcb.sensenet.internal.util.RandomUtil;
import com.tcb.common.util.SafeMap;
import com.tcb.cytoscape.cyLib.cytoApiWrappers.CyNetworkAdapter;

import network.AdjacencyMatrixNetworkFactory;
import network.WeightedTestNetwork;
import network.WeightedTestNetworkFactory;
import testUtils.DummyRandomUtil;

public class NetworkWalksTest {

	private Integer[][] matrix = {
			{0,0,2,0,0,1,1,0,0,1},
			{0,0,0,0,4,0,0,0,0,1},
			{0,0,0,0,0,0,1,1,0,0},
			{0,0,0,0,1,1,0,1,0,1},
			{0,0,0,0,0,0,1,1,1,0},
			{0,0,0,0,0,0,0,0,0,1},
			{0,0,0,0,0,0,0,1,0,0},
			{0,0,0,0,0,0,0,0,0,1},
			{0,0,0,0,0,0,0,0,0,3},
			{0,0,0,0,0,0,0,0,0,0}};
	private Double[] weightsArr = {
			1.,1.,1.,1.,1.,
			1.,1.,1.,1.,1.
	};
	private List<Double> dummyRandoms = Arrays.asList(
			0.3132,0.3132,0.555979,0.555979,
			0.938285,0.938285,0.736322,0.736322,
			0.192408,0.19514,0.95096,0.192408,
			0.290445,0.19514,0.819082,0.95096,
			0.48821,0.290445,
			0.67, 0.67, 0.32, 0.32
			);
	
	protected CyNode source;
	private RandomUtil rnd;
	private SafeMap<CyNode,Double> weights;
	private List<CyNode> nodes;
	private CyNetworkAdapter network;
	
	@Before
	public void setUp() throws Exception {
		this.network = new AdjacencyMatrixNetworkFactory().create(matrix);
		this.nodes = network.getNodeList().stream()
				.sorted(Comparator.comparing(n -> n.getSUID()))
				.collect(Collectors.toList());
		this.source = nodes.get(0);
		this.rnd = new DummyRandomUtil(dummyRandoms);
		this.weights = new SafeMap<>();
		for(int i=0;i<weightsArr.length;i++) {
			weights.put(nodes.get(i), weightsArr[i]);
		}
	}
	
	@Test
	public void testRunNetworkWalks() {
		StepStrategy stepStrategy = new WeightedRandomStep(rnd,weights);
		NetworkWalkStrategy walkStrategy = new NetworkWalk(rnd,0.25);
		
		Map<CyNode,Integer> visited = new NetworkWalks().runNetworkWalks(walkStrategy, stepStrategy, 10,
				network, source, 10, true);
						
		// Reference: Cpp implementation, git commit 29f3925b3e0ac64281b3caab95bef9cb2dbeb20
		Map<CyNode,Integer> ref = new SafeMap<>();
		ref.put(nodes.get(4),5);
		ref.put(nodes.get(3),2);
		ref.put(nodes.get(1),8);
		ref.put(nodes.get(9),8);
		ref.put(nodes.get(7),9);
		ref.put(nodes.get(6),7);
		ref.put(nodes.get(5),2);
		ref.put(nodes.get(2),8);
		ref.put(nodes.get(0),10);
		
		assertEquals(ref,visited); 
	}
	
	@Test
	public void testRunNetworkWalks2() {
		weights.replace(nodes.get(9), 0.);
		weights.replace(nodes.get(3), 3.);
		weights.replace(nodes.get(5), 5.);
		
		StepStrategy stepStrategy = new WeightedRandomStep(rnd,weights);
		NetworkWalkStrategy walkStrategy = new NetworkWalk(rnd,0.25);
		
		Map<CyNode,Integer> visited = new NetworkWalks().runNetworkWalks(walkStrategy, stepStrategy, 10,
				network, source, 10, true);
						
		// Reference: Cpp implementation, git commit 29f3925b3e0ac64281b3caab95bef9cb2dbeb20
		Map<CyNode,Integer> ref = new SafeMap<>();
		ref.put(nodes.get(4),9);
		ref.put(nodes.get(3),10);
		ref.put(nodes.get(7),10);
		ref.put(nodes.get(6),8);
		ref.put(nodes.get(5),10);
		ref.put(nodes.get(2),8);
		ref.put(nodes.get(0),10);
		
		assertEquals(ref,visited); 
	}
}
