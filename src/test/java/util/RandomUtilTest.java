package util;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import com.tcb.sensenet.internal.util.RandomUtil;

public class RandomUtilTest {

	private List<Integer> lst;
	private RandomUtil rnd;



	@Before
	public void setUp() throws Exception {
		this.lst = Arrays.asList(0,1,2,3,4);
		this.rnd = new RandomUtil();
		rnd.setSeed(512l);
	}

	@Test
	public void testPickRandom() {
		int[] results = new int[5];
		int[] ref = new int[]{10100,10020,9861,10116,9903};
		for(int i=0;i<50000;i++){
			Integer test = rnd.pickRandom(lst);
			results[test]+=1;
		}
		
		for(int i=0;i<ref.length;i++){
			assertEquals(ref[i],results[i]);
		}
	}
	
	@Test
	public void testPickRandomWeighted() {
		int[] results = new int[5];
		int[] ref = new int[]{9185,18091,9209,4484,9031};
		for(int i=0;i<50000;i++){
			Integer test = rnd.pickRandom(lst, Arrays.asList(1.,2.,1.,.5,1.));
			results[test]+=1;
		}

		for(int i=0;i<ref.length;i++){
			assertEquals(ref[i],results[i]);
		}
	}
	
}
