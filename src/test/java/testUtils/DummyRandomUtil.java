package testUtils;

import java.util.List;

import com.tcb.sensenet.internal.util.RandomUtil;

public class DummyRandomUtil extends RandomUtil {
	
	private List<Double> values;
	private int idx;

	public DummyRandomUtil(List<Double> values) {
		this.values = values;
		idx = 0;
	}
	
	@Override
	public Double nextDouble() {
		if(idx >= values.size()) idx = 0;
		double x = values.get(idx);
		idx++;
		return x;
	}
}
